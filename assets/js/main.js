$(document).ready(function() {
	$("#forgotUsername").click(function(){
		$('#loginDetail').hide();
		$('#forgotDetail').show();
	});
	$("#back").click(function(){
		$('#loginDetail').show();
		$('#forgotDetail').hide();
	});

	var toggle = true;

	$(".sidebar-icon").click(function (event) {
    event.stopPropagation();
		if (!toggle) {
			$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
			$("#menu span").css({
				"position": "absolute"
			});
			$('.sidebar-icon').html('<span class="fa fa-bars"></span>');
		} else {
      $(".modules-menu").hide();
			$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
			setTimeout(function () {
				$("#menu span").css({
					"position": "relative"
				});
			}, 100);
			$('.sidebar-icon').html('<span class="fa fa-times"></span>');
		}
		toggle = !toggle;
	});

	$('#menu').on('mouseenter', function () {
    $(".modules-menu").hide();
		$(".page-container").addClass("sidebar-collapsed-back");
		setTimeout(function () {
			$("#menu span").css({
				"position": "relative"
			});
		}, 100);
		$('.sidebar-icon').html('<span class="fa fa-times"></span>');
	})

	// modules
	$(".modules-icon").click(function (event) {
    event.stopPropagation();
    $(".page-container").removeClass("sidebar-collapsed-back");
		$(".modules-menu").fadeToggle("fast", "linear");
	});
});


$(document).mouseup(e => {
  var $sidebar = $('.page-container');
  var $modules = $('.modules-menu');

  if (!$sidebar.is(e.target) && $sidebar.has(e.target).length === 0) {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({
      "position": "absolute"
    });
    $('.sidebar-icon').html('<span class="fa fa-bars"></span>');
  }

  if (!$modules.is(e.target) && $modules.has(e.target).length === 0) {
    $(".modules-menu").hide();
  }
});

// enlarge screen
var elem = document.documentElement;
function openFullscreen() {
	if (elem.requestFullscreen) {
		elem.requestFullscreen();
	} else if (elem.webkitRequestFullscreen) { /* Safari */
		elem.webkitRequestFullscreen();
	} else if (elem.msRequestFullscreen) { /* IE11 */
		elem.msRequestFullscreen();
	}
}

function closeFullscreen() {
	if (document.exitFullscreen) {
		document.exitFullscreen();
	} else if (document.webkitExitFullscreen) { /* Safari */
		document.webkitExitFullscreen();
	} else if (document.msExitFullscreen) { /* IE11 */
		document.msExitFullscreen();
	}
}

// chat
$(function() {
  var INDEX = 0;
  $("#chat-submit").click(function(e) {
    e.preventDefault();
    var msg = $("#chat-input").val();
    if(msg.trim() == ''){
      return false;
    }
    generate_message(msg, 'self');
    var buttons = [
        {
          name: 'Existing User',
          value: 'existing'
        },
        {
          name: 'New User',
          value: 'new'
        }
      ];
    setTimeout(function() {
      generate_message(msg, 'user');
    }, 1000)

  })
  // <span class="image-cropper pl-2">
  //         <img src="images/Profile Picture.png" alt="user" class="profile-pic">
  //       </span>

  function generate_message(msg, type) {
    INDEX++;
    var str="";
    str += "<div id='cm-msg-"+INDEX+"' class=\"chat-msg "+type+"\">";
    str += "          <span class=\"msg-avatar\">";
    str += "            <img src=\"images/Profile Picture.png\">";
    str += "          <\/span>";
    str += "          <div class=\"cm-msg-text\">";
    str += msg;
    str += "          <\/div>";
    str += "        <\/div>";
    $(".chat-logs").append(str);
    $("#cm-msg-"+INDEX).hide().fadeIn(300);
    if(type == 'self'){
     $("#chat-input").val('');
    }
    $(".chat-logs").stop().animate({ scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
  }

  function generate_button_message(msg, buttons){
    /* Buttons should be object array
      [
        {
          name: 'Existing User',
          value: 'existing'
        },
        {
          name: 'New User',
          value: 'new'
        }
      ]
    */
    INDEX++;
    var btn_obj = buttons.map(function(button) {
       return  "              <li class=\"button\"><a href=\"javascript:;\" class=\"btn btn-primary chat-btn\" chat-value=\""+button.value+"\">"+button.name+"<\/a><\/li>";
    }).join('');
    var str="";
    str += "<div id='cm-msg-"+INDEX+"' class=\"chat-msg user\">";
    str += "          <span class=\"msg-avatar\">";
    str += "            <img src=\"https:\/\/image.crisp.im\/avatar\/operator\/196af8cc-f6ad-4ef7-afd1-c45d5231387c\/240\/?1483361727745\">";
    str += "          <\/span>";
    str += "          <div class=\"cm-msg-text\">";
    str += msg;
    str += "          <\/div>";
    str += "          <div class=\"cm-msg-button\">";
    str += "            <ul>";
    str += btn_obj;
    str += "            <\/ul>";
    str += "          <\/div>";
    str += "        <\/div>";
    $(".chat-logs").append(str);
    $("#cm-msg-"+INDEX).hide().fadeIn(300);
    $(".chat-logs").stop().animate({ scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
    $("#chat-input").attr("disabled", true);
  }

  $(document).delegate(".chat-btn", "click", function() {
    var value = $(this).attr("chat-value");
    var name = $(this).html();
    $("#chat-input").attr("disabled", false);
    generate_message(name, 'self');
  })

  $("#chat-circle").click(function() {
    $("#chat-circle").toggle('scale');
    $(".chat-box").toggle('scale');
  })

  $(".chat-box-toggle").click(function() {
    $("#chat-circle").toggle('scale');
    $(".chat-box").toggle('scale');
  })

})